'use strict'

//var debug = require('debug')
const compression = require('compression')
const express = require('express')
const morgan = require('morgan')
const path = require('path')
const config = require(path.join(__dirname, 'config.js'))

const app = express()

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))
app.set('config', config)
app.set('baseDir', __dirname)
//debug(config)

// compress all requests
app.use(compression())

// logging
app.use(morgan('API :url'))

// static
app.use(express.static('public'))

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  res.header('Cache-Control', 'public,max-age=604800') // one week
  next()
})

/* Add routes */
// API
var apiv1 = require(path.join(__dirname, 'routes', 'apiv1.js'))
app.use('/v1', apiv1)
var apiv2 = require(path.join(__dirname, 'routes', 'apiv2'))
app.use('/v2', apiv2)

// Docs
let v1 = function (req, res) {
  // Reverse datasetAliases
  // {foo: 'a', bar: 'a', baz: 'b'} => {a: ["foo", "bar"], b: ["baz"]}
  const reverseMapFromMap = map => Object.keys(map).reduce((acc, k) => {
    acc[map[k]] = (acc[map[k]] || []).concat(k)
    return acc
  }, {})
  res.render('doc-v1', {
    pageData: {
      modules: Object.keys(config.modules),
      modulesettings: config.modules,
      datasets: config.datasets,
      datasetAliases: reverseMapFromMap(config.datasetAliases),
      languageFallbacks: config.languageFallbacks
    }
  })
}
let v2 = function (req, res) {
  res.render('doc', {
    pageData: {
      modules: Object.keys(config.modules),
      modulesettings: config.modules,
      datasets: config.datasets,
      languageFallbacks: config.languageFallbacks,
    }
  })
}
app.get('/doc/v1/?', v1)
app.get('/doc/:anything?', v2)
app.get('/', (req, res, next) => {
  res.redirect(301, '/doc/v2/')
})

// Errors
app.use(function (req, res, next) {
  res.status(404).send('Sorry, can\'t find that!')
})

// Start server
const server = app.listen(process.env.PORT || 3000, function () {
  let host = server.address().address
  let port = server.address().port

  console.log('Thenmap API listening at http://%s:%s', host, port)
})
