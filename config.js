const variant_desc = "Chose a particular variant of a border, or set of borders. E.g. `somalia=1` or `somalia=2`. Multiple variants can be semicolon separated."
const language_dec = "Language for metadata properties, map labels etc."

var config = {
  apiVersion: "v1",
  modules: {
    "data": {
      contentType: "application/json;charset=utf-8",
      file: "data.js",
      description: "Returns data (such as names and dates) on political entities.",
      parameters2: {
        get: {
          data_props: {
            description: "Data to retrieve foreach entity. Available properties will depend on the dataset, but `shapeid` (corresponding shape in the geo module), `name`, `sdate` (first date the entity should be present on a map), `edate` (last date), will always be available. Use the `info` modul to find others.",
            default: ["id", "name"],
            multiple: true,
          },
          data_format: {
            description: "Format of returned data",
            default: "json",
            allowed: ["json", "csv"],
          },
        }
      },
      parameters: {
        get: {
          data_props: {
            description: "Data to retrieve. Will depend on the dataset, but `shapeid` (corresponding shape in the geo module), `name`, `sdate` (first date the entity should be present on a map), `edate` (last date), will always be available (use the `info` modul to find out more). Properties will be grouped by shape, and each shape will have an array of one or more political entities connected to it.",
            default: "id"
          },
          data_lang: {
            description: "Laguage to use for translatable properties, such as name. Available and default languages will depend on dataset chosen (use the `info` modul to find out more)."
          },
          language: {
            description: "Deprecated. Use data_lang.",
            deprecated: true,
          },
          data_variant: {
            description: variant_desc
          }
        }
      }
    },
    "info": {
      contentType: "application/json;charset=utf-8",
      file: "info.js",
      description: "Returns metadata on a dataset.",
      parameters2: {
        get: {}
      }
    },
    "geo": {
      contentType: "application/json;charset=utf-8",
      file: "geo.js",
      description: "Returns geodata, as geojson or topojson.",
      parameters2: {
        get: {
          geo_type: {
            description: "Format. Topojson is significantly smaller, but will take longer for the API to produce.",
            default: "geojson",
            allowed: ["geojson", "topojson"]
          },
          geo_crs: {
            description: "Coordinate reference system. Anything other that wgs84 will be reprojected, and therefore slower.",
            default: "wgs84",
          },
          geo_scale: {
            description: "¡DEPRECATED! Scale. Use a larger scale for very large or zoomable maps. Deprecation warning: This feature will be removed, and replaced with something more exact. The definitions of s, m, and l are not very clear, and varies from dataset to dataset; and the current implementation is shaky.",
            default: "s",
            allowed: ["s", "m", "l"]
          },
          geo_props: {
            description: "Data from the data module, to include in shape properties",
            multiple: true,
          },
        }
      },
      parameters: {
        get: {
          geo_type: {
            description: "Format. Topojson is significantly smaller, but will take slightly longer for the API to produce.",
            default: "geojson",
            allowed: ["geojson", "topojson"]
          },
          geo_crs: {
            description: "Coordinate reference system. Anything other that wgs84 will be reprojected, and therefore slower.",
            default: "wgs84",
            allowed: ["sweref99", "wgs84", "rt90", "tm35fin", "kkj", "etrs89"] //"ch1903"
          },
          geo_variant: {
            description: variant_desc
          },
          geo_scale: {
            description: "Scale. Use a larger scale for very large or zoomable maps.",
            default: "s",
            allowed: ["s", "m", "l"]
          },
          geo_props: {
            description: "Data from the data module, to include in shape properties",
            default: ""
          },
          geo_lang: {
            description: "Laguage to use for translatable properties, such as name. Available and default languages will depend on dataset chosen (use the `info` modul to find out more).",
          },
          geo_flatten_props: {
            description: "Create a flat entity/property structure, rather than a two or three dimensional? There will not always be a one-to-one relation between geoshapes and data entities, but if you are fetching geo data from only one specific date, it might be useful to flatten the properties. Set this to `true` if you want to use the geodata in e.g. CartoDB. This will flatten not only entities, but also their properties.",
            allowed: ["true", "false"],
            default: "false"
          }
        }
      }
    },
    "svg": {
      contentType: "image/svg+xml;charset=utf-8",
      file: "svg.js",
      description: "Returns an SVG representation of the borders from a dataset at a certain time.",
      parameters2: {
        get: {
          svg_proj: {
            description: "Defaults to the first recommended projection for this dataset. Use the info module to see all recommended projections.",
          },
          svg_width: {
            description: "Width in pixels.",
            default: 600
          },
          svg_height: {
            description: "Height in pixels.",
            default: 600
          },
          svg_props: {
            description: "Data from the data module, to include as data attributes",
            default: ""
          },
        }
      },
      parameters: {
        get: {
          svg_proj: {
            description: "Defaults to the first recommended projection for this dataset. Use the info module to see all recommended projections.",
          },
          projection: {
            description: "Deprecated. Use svg_proj.",
            deprecated: true,
          },
/*          mode: {
            description: "Should we return a fully functional SVG images, or an array of paths for further processing?",
            allowed: ["paths", "full"],
            default: "full"
          },*/
          width: {
            description: "Deprecated. Use svg_width.",
            deprecated: true,
          },
          height: {
            description: "Deprecated. Use svg_height.",
            deprecated: true,
          },
          svg_width: {
            description: "Used for viewport size, and svg width attribute. (px)",
            default: 600
          },
          svg_height: {
            description: "Used for viewport size, and svg height attribute. (px)",
            default: 600
          },
          svg_variant: {
            description: variant_desc
          },
          svg_props: {
            description: "Data from the data module, to include as data attributes",
            default: ""
          },
          svg_lang: {
            description: "Laguage to use for translatable properties, such as name. Available and default languages will depend on dataset chosen (use the `info` modul to find out more).",
          },
        }
      }
    }
  },
  datasets: {
    /*
      bbox: always use WGS 84
      recommendedProjections: The first of these this be used as default for SVG maps
    */
    "fi-8": {
      description: "Finnish municipalities, from 2010",
      bbox: [19.1, 59.5, 31.6, 70.1],
      languages: ["sv", "fi", "en", "ru", "se", "et", "no"],
      defaultLanguage: "fi",
      recommendedProjections: ["tm35fin"],
      parameters: {
        language: {
          description: language_dec,
          default: "fi",
          allowed: ["sv", "fi", "en", "ru", "se", "et", "no"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "ch-8": {
      description: "Swiss municipalities, from 2010. Data source: Federal Office of Topography. Please note the source when reusing the data.",
      bbox: [5.9,45.8,10.5,47.9],
      languages: ["als", "frp", "lmo", "de", "fr", "it", "rm", "en", "sq", "es", "tr", "pt"],
      defaultLanguage: "de",
      recommendedProjections: ["swissgrid"],
      parameters: {
        language: {
          description: language_dec,
          default: "de",
          allowed: ["als", "frp", "lmo", "de", "fr", "it", "rm", "en", "sq", "es", "tr", "pt"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "no-7": {
      description: "Norwegian municipalities, from 2006",
      bbox: [4, 57.5, 38, 70],
      languages: ["nb", "nn", "no", "en", "se", "fi", "da"],
      defaultLanguage: "nb",
      recommendedProjections: ["euref89no"],
      parameters: {
        language: {
          description: language_dec,
          default: "nb",
          allowed: ["nb", "nn", "en", "se", "fi", "da"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "no-4": {
      description: "Norwegian counties, from 1919",
      bbox: [4, 57.5, 38, 70],
      languages: ["nb", "nn", "no", "en", "se", "fi", "da"],
      defaultLanguage: "nb",
      recommendedProjections: ["euref89no"],
      parameters: {
        language: {
          description: language_dec,
          default: "nb",
          allowed: ["nb", "nn", "en", "se", "fi", "da"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "dk-7": {
      description: "Danish municipalities, from 1970",
      bbox: [7.7, 54.7, 15.5, 57.9],
      languages: ["da", "de", "en", "sv", "ffr", "nds", "fo"],
      defaultLanguage: "da",
      recommendedProjections: ["euref89dk"],
      parameters: {
        language: {
          description: language_dec,
          default: "da",
          allowed: ["da", "de", "en", "sv", "ffr", "nds", "fo"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "se-7": {
      description: "Swedish municipalities, from 1974 (a few borders in southern Sweden still missing from 1973)",
      bbox: [10, 54, 25, 70],
      languages: ["sv", "en", "fi", "se"],
      defaultLanguage: "sv",
      recommendedProjections: ["sweref99tm"],
      parameters: {
        language: {
          description: language_dec,
          default: "sv",
          allowed: ["sv", "en", "fi", "se"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "se-4": {
      description: "Swedish counties, from 1968",
      bbox: [10, 54, 25, 70],
      languages: ["sv", "en", "fi", "se"],
      defaultLanguage: "sv",
      recommendedProjections: ["sweref99tm"],
      parameters: {
        language: {
          description: language_dec,
          default: "sv",
          allowed: ["sv", "en", "fi", "se"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "us-4": {
      description: "US states",
      bbox: [-125,24,-67,49],
      languages: ["sv", "en", "es", "zh", "fr", "de", "it", "nl", "tl", "vi", "ik", "ko", "ru", "fa", "nv", "th", "chr", "ar"],
      defaultLanguage: "en",
      recommendedProjections: ["albersUsa"],
      parameters: {
        language: {
          description: language_dec,
          default: "en",
          allowed: ["sv", "en", "es", "zh", "fr", "de", "it", "nl", "tl", "vi", "ik", "ko", "ru", "fa", "nv", "th", "chr", "ar"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "gl-7": {
      description: "Municipalities of Greenland since the home rule",
      bbox: [-74,59.5,-14,84],
      languages: ["kl", "da", "sv", "no", "en"],
      defaultLanguage: "kl",
      recommendedProjections: ["gr96"],
      parameters: {
        language: {
          description: language_dec,
          default: "kl",
          allowed: ["kl", "da", "sv", "no", "en"],
        },
        variant: {
          description: variant_desc,
        },
      },
    },
    "world-2": {
      description: "Countries of the world. Generally speaking, de-facto independent nation are included, even if when they enjoy limited international recognition.",
      bbox: [-180, -90, 180, 90],
      languages: ["sv","en","fi","fr","de","es","ru","it","nl","pl","zh","pt","ar","ja","fa","nn","no","he","tr","da","uk","ca","id","hu","vi","ko","et","cs","hi","sr","bg", "nn"],
      defaultLanguage: "en",
      recommendedProjections: ["robinson", "mollweide"],
      parameters: {
        language: {
          description: language_dec,
          default: "en",
          allowed: ["sv","en","fi","fr","de","es","ru","it","nl","pl","zh","pt","ar","ja","fa","nn","no","he","tr","da","uk","ca","id","hu","vi","ko","et","cs","hi","sr","bg", "nn"],
        },
        variant: {
          description: variant_desc,
        },
      },
    }
  },
  datasetAliases: {
    "sweden-7": "se-7",
    "sweden7": "se-7",
    "se7": "se-7",
    "sweden": "se-7",
    "se": "se-7",

    "finland-8": "fi-8",
    "finland8": "fi-8",
    "fi8": "fi-8",
    "finland": "fi-8",
    "fi": "fi-8",

    "sweden-4": "se-4",
    "sweden4": "se-4",
    "se4": "se-4",

    "norway": "no-7",
    "norway-7": "no-7",
    "norway7": "no-7",
    "no": "no-7",
    "no7": "no-7",
    "norway-4": "no-4",
    "norway4": "no-4",
    "no4": "no-4",

    "world": "world-2",

    "us4": "us-4",
    "us": "us-4",

    "ch8": "ch-8",
    "switzerland": "ch-8",
    "switzerland-8": "ch-8",
    "switzerland8": "ch-8",
    "ch": "ch-8",
  },
  languageFallbacks: { //https://github.com/wikimedia/jquery.i18n/blob/master/src/jquery.i18n.fallbacks.js
    fit: "fi",
    fkv: "fi",
    nn: "nb",
    no: "nb",
    nn: "nn",
    sr: "sr-ec",
    uk: "ru",
    zh: "zh-hans",
    kl: "da",
    li: "nl",
    wa: "fr",
    als: "de",
    frp: "fr",
    lmo: "it",
    nds: "de",
  },
  crss_aliases: {
    sweref99tm: "EPSG:3006",
    sweref99: "EPSG:3006",
    rt90: "EPSG:3021",
    wgs84: "EPSG:4326",
    tm35fin: "EPSG:3067",
    gr96: "EPSG:6064",
    kkj: "EPSG:2393",
    ch1903: "EPSG:4150",
    etrs89: "EPSG:4258",
    euref89no: "EPSG:5110",
    ngo1948: "EPSG:4273",
    euref89dk: "EPSG:4096",
    swissgrid: "EPSG:2056",
    albersUsa: "EPSG:42303",
    robinson: "ESRI:54030",
    mercator: "EPSG:3857",
    // eckert4: "ESRI:54012", // Completely missing in Proj4js
    winkel3: "ESRI:54019", // Doesnt work in Proj4, yet?
    mollweide: "ESRI:54009",
    // kavrayskiy7: "",  // Sadly missing
    // wagner6: "",
  },
}

module.exports = config
