/* Various useful functions */
const request = require('request')
const {reproject} = require("reproject")
let epsg = {
  // Projections not in the epsg lib can be added here
  'EPSG:5110': "+proj=tmerc +lat_0=58 +lon_0=10.5 +k=1 +x_0=100000 +y_0=1000000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
  'EPSG:6064': "+proj=lcc +lat_1=70.33333333333333 +lat_2=67 +lat_0=68.68747555555557 +lon_0=-20 +x_0=30500000 +y_0=6500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs",
  'EPSG:42303': "+proj=aea +lat_1=29.5 +lat_2=45.5 +lat_0=23 +lon_0=-96 +x_0=0 +y_0=0 +datum=NAD83 +units=m +no_defs",
  'EPSG:5500': "+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +geoidgrids=g2012a_conus.gtx,g2012a_alaska.gtx,g2012a_guam.gtx,g2012a_hawaii.gtx,g2012a_puertorico.gtx,g2012a_samoa.gtx +vunits=m +no_defs",
  'ESRI:54030': "+proj=robin +lon_0=0 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs",
  'ESRI:54009': "+proj=moll +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs",
  'EPSG:4269': 'GEOGCS["NAD83",DATUM["North_American_Datum_1983",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],AUTHORITY["EPSG","6269"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4269"]]',
  'ESRI:54012': "+proj=eck4 +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m no_defs",
  'ESRI:54019': "+ellps=WGS84 +datum=WGS84 +units=m no_defs",
}
Object.assign(epsg, require("epsg"))

exports.epsg = epsg

/**
 * Lightweight request parameters parsing.
 *
 * @param {object} reqParams are request params from Node/Express
 * @param {object} optParams are of the following format:
 * {
 *   fileFormat: {
 *	   default: "csv",
 *	   allowed: ["csv", "xlsx"],
 *     multiple: false,
 *	 },
 * }
 * @return {object} parameters that are valid, or defaults
 */
exports.parseParams = function(reqParams, optParams){

	let opts = Object.keys(reqParams)
		// Only allowed parameters
		.filter(k=>k in optParams)
		// Return as an object
		.reduce((o, k)=>{
			// Split multiparams by "|"
			if (optParams[k].multiple){
				o[k]=reqParams[k].split("|")
				if (optParams[k].allowed){
					o[k] = o[k].filter(x => optParams[k].allowed.includes(x))
				}
			} else {
				if (optParams[k].allowed){
					if (optParams[k].allowed.includes(reqParams[k])) {
						o[k] = reqParams[k]
					}
				} else {
					o[k] = reqParams[k]
				}
			}
			return o
		}, {})

	let defaults = Object.keys(optParams)
		.reduce((o, k)=>{o[k]=optParams[k].default;return o}, {})
	return Object.assign({}, defaults, opts)

}

/**
 * Call an endpoint at this very API
 *
 * @param {object} request
 * @param {string} version
 * @param {string} dataset
 * @param {string} module
 * @param {string} date
 * @param {object} params
 * @param {function} callback
 * @return {string} responsebody
 */
exports.callHome = (req, version, dataset, module_, date, params, callback) => {
	let urlParams = Object.entries(params).map(x=>x.join("=")).join("&")
	request(`${req.protocol}://${req.get('host')}/${version}/${dataset}/${module_}/${date}?${urlParams}`, (err, res, body) => callback(err, body))
}

/**
 * Convert an X/Y coordinate from one CRS to another:
 * convert([10, 15], "wgs84", ""EPS:3006") =>
 *
 * @param {Array} coordinate
 * @param {string} from
 * @param {string} to
 * @return {Array} coordinate
 */
exports.convert = (coordinate, from_, to) => {
	return reproject({type: "Point", coordinates: [coordinate[0], coordinate[1]]}, from_, to, epsg).coordinates
}

exports.getParam = function() {
  /* Return the first of any number of arguments that is not undefined */
  /* Used to get a parameter from multiple places */
  let args = [].slice.call(arguments)  // put all args into array
  return args.reduce((x, a)=>x !== undefined ? x : a)
}

exports.getParams = function() {
  /* Return the first of any number of arguments that is not undefined */
  /* Used to get a parameter from multiple places */
  /* Will return an array, by splitting resulting argument on `|` if needed*/
  let args = [].slice.call(arguments)  // put all args into array
  while (args.length) {
    var arg = args.shift();
    if (arg !== undefined) {
      if (typeof(arg) === 'string'){
        return arg.split('|');
      } else if (arg instanceof Array) {
        return arg;
      } else {
        return [arg];
      }
    }
  }
}
