/* This module will wrap the v2 endpoint, to avoid using hopelessly outdate
libraries of the old svg creator. We try to reproduce the old format here.
*/
var Api = require(__dirname + '/baseModule.js')
Api._cache = {}
const {callHome, getParam} = require('../utils')

module.exports = function(ApiParams) {

  var api = new Api("svg", ApiParams)

  api.get = function(callback) {
    var self = this
    this.datasetConfig = this.apiParams.config.datasets[this.apiParams.dataset]
    this.getCallback = callback

    //Use the first recommended projection for this dataset, if none given
    this.projection = this.parameters["projection"] || this.parameters["svg_proj"]
                     || this.datasetConfig.recommendedProjections[0]
    this.width = this.parameters["width"] || this.parameters["svg_width"]
    this.height = this.parameters["height"] || this.parameters["svg_height"]
    this.lang = this.parameters["svg_lang"] || null
    this.variant = this.parameters["svg_variant"] || null

    /* Round up dimension for a better chance of caching */
    this.width = Math.ceil(this.width / 20.0) * 20
    this.height = Math.ceil(this.height / 20.0) * 20

    this.date = this.apiParams.date || "*" // v2 requires a date, but can parse a wildcard

    this.language = getParam(this.parameters["language"], this.parameters["svg_lang"], this.defaultLanguage)

    let urlParams = {
      svg_props: this.parameters.svg_props,
      language: this.language,
      svg_crs: getParam(this.parameters.svg_proj, "wgs84"),
      svg_height: this.height,
      svg_width: this.width,
      //variant: //FIXME
    }
    callHome(this.apiParams.req, "v2", this.apiParams.dataset, "svg", this.date, urlParams, (err, body) => {
      body = body.replace(/thenmap:([^=]*)/g, "data-$1_1")
      body = body.replace(/id=\"(\d+)\"/g, "data-id_1=\'$1\' id=\'$1\'")
      self.apiParams.res.json({
        svg: body
      })
    })



  }
  return api
}
