const path = require('path')
const router = require('express').Router()
const {getParam, parseParams} = require(path.join(__dirname, 'utils'))

router.get('/:dataset/:module/:date', function (req, res, next) {

  const config = req.app.get('config')

  /* Check if dataset exists or return */
  let dataset = req.params.dataset
  if (dataset in config.datasetAliases) {
    dataset = config.datasetAliases[dataset]
  }
  if (!(dataset in config.datasets)) {
    res.status(501).send('Sorry, that dataset does not exist (yet!) Available datasets are: ' + Object.keys(config.datasets).join(', '))
    return
  }

  /* Check that module exists  */
  var modulename = req.params.module
  if (!(modulename in config.modules)){
    res.status(401).send('No valid module specified. Available modules are: ' + Object.keys(config.modules).join(', '))
    return
  }

  let datasetConfig = config.datasets[dataset]
  let moduleConfig = config.modules[modulename]

  let fromDate = toDate = date = req.params.date
  if (date === "*") {
    fromDate = "0"
    toDate = "99"
  } else {
    /* Check that date is valid  */
    try {
      date = new Date(date).toISOString()
    } catch(e) {
      res.status(401).send(`<code>${date}</code> is not a valid date. Try something like "2016", "2016-05", or "2016-05-06".`)
      return
    }
  }

  /* Check that language is valid  */
  if (req.query.language && !datasetConfig.languages.includes(req.query.language)){
    res.status(401).send(`<code>${req.query.language}</code> is not a valid language for this dataset. Available languages are: <code>${datasetConfig.languages}</code>`)
    return
  }

  /* set general parameters */
  let apiParams = {
    dataset: dataset,
    date: date,
    fromDate: fromDate,
    toDate: toDate,
    config: config,
  }

  /* set user provided parameters */
  let availableParams = Object.assign({}, moduleConfig.parameters2.get, datasetConfig.parameters)
  let reqParams = parseParams(req.query, availableParams)
  Object.assign(apiParams, reqParams)

  /* Expand variants  */
  // "a=x;b=y" => {a: "x", b: "y"}
  apiParams.variant = getParam(apiParams.variant, "")
    .split(";").map(x=>x.split("="))
    .reduce((o, [k, v])=>(o[k]=v, o), {})

  /* Load module */
  require(path.join(__dirname, 'apiv2', moduleConfig.file))
    .get(apiParams, req, res, next)

})

module.exports = router
