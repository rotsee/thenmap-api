const{readFile} = require('fs')
//var Proj4 = require('proj4')
//var Epsg = require('../../resources/epsg.js')(Proj4)
const {reproject} = require("reproject")
const request = require('request')
const {epsg, getParam, callHome} = require('../utils')

let geo = {}

function cleanup (geodata){
  // Clean up feature, including only the ID property
  // TODO remove, but need to have a look at shapefiles first
  let features = []
  for (feature of geodata.features){
    let cleanedFeature = {
      type: "Feature",
      properties: {
        id: getParam(feature.properties.ID, feature.properties.id)
      },
      geometry: feature.geometry,
    }
    features.push(cleanedFeature)
    //if (cleanedFeature.geometry.coordinates.every(x=>x.length)){
    //}
  }
    geodata.features = features
  return geodata

}

geo.get = function(apiParams, req, res, next) {

  const ver = "1"  // Dataset version. Not implemeted, every dataset is on v1
  let res_ = res
  let defaultCrs = "EPSG:4326"  // WGS84
  let crs = getParam(apiParams.config.crss_aliases[apiParams.geo_crs], apiParams.geo_crs)
  if (!(crs in epsg)){
    res.status(404).send(`Sorry, the requested CRS "${crs}" is not known to Thenmap.`)
    return
  }

  // Get geodata
  let filename = ver + "-" + apiParams["geo_scale"]
  let filepath = `${__dirname}/../../testdata/${apiParams.dataset}/${filename}.geojson`;
  readFile(filepath, 'utf-8', (err, geodata) => {
    if (err) {
      console.log("Error reading geodata file", err)
      return
    }
    geodata = JSON.parse(geodata)

    // Call data module, so we can filter the result by date and/or variant
    let urlParams = {
      // make sure needed props are included
      data_props: (apiParams.geo_props || []).concat(["id", "sdate", "edate", "shapeid"]).join("|"),
      language: apiParams.language,
      //variant: //FIXME
    }
    callHome(req, "v2", apiParams.dataset, "data", apiParams.date, urlParams, (err, body) => {
      if (err){
        console.log("Could not call data module: " + err)
      }
      // Make shape-id the key
      let datadata = JSON.parse(body).reduce((o, k)=>(o[k.shapeid]=k, o), {})
      function merge(o1, o2){
        // Merge objects, for when using wildcard dates, and we have multiple id's on one shape
        if (!o1){
          return o2
        }
        Object.keys(o1).forEach(k => {
          if ((k in o2) && (o2[k] !== o1[k])){
            if (!Array.isArray(o1[k])){
              o1[k] = [o1[k]]
            }
            if (!Array.isArray(o2[k])){
              o1[k].push(o2[k])
            } else {
              o1[k] = o1[k].concat(o2[k])
            }
            o1[k] = [...new Set(o1[k])]
          }
        })
        return o1
      }
      datadata = JSON.parse(body).reduce((o, k)=>(o[k.shapeid]=merge(o[k.shapeid], k), o), {})

      // cleanup
      geodata = cleanup(geodata)
      // filter geodata
      geodata.features = geodata.features.filter(function (feature) {
        return (parseInt(feature.properties.id) in datadata)
      })

      //Add properties
      if (apiParams.geo_props) {
        var features = [];
        for (feature of geodata.features){
          var featureId = feature.properties.id
          var newFeature = {
            "type":"Feature",
            "properties": datadata[featureId],
            "geometry": feature.geometry
          }
          features.push(newFeature)
        }
        geodata.features = features
      }

      // Reprojection
      if (crs !== defaultCrs){
        console.log("Rerender from/to", defaultCrs, crs)
        try {
          geodata = reproject(geodata, defaultCrs, crs, epsg)
        } catch(error) {
          res.status(501).send(`The requested CRS or projection "${crs}" is known to Thenmap, but we cannot yet handle it.`)
          return
        }
      }

      // Return geojson or topojson
      if (apiParams.geo_type === "geojson"){
        res_.json(geodata)
      } else if (apiParams.geo_type === "topojson"){
        var Topojson = require("topojson");

        var geojson = JSON.parse(JSON.stringify(geodata))//deep copy
        var topojson = Topojson.topology({collection: geojson},
          { // options
            id: function(d) {
              return d.properties.id
            },
            "property-transform": function (feature) {
              return feature.properties
            }
          }
        )
        res_.json(topojson)
      } else {
        return (console.log("Invalid geo_format (this should never happen)"))
      }

    }) // callHome

  })

}
module.exports = geo
