const {epsg, getParam, callHome, convert} = require('../utils')
const geojson2svg = require('geojson2svg')

let svg = {}
svg.get = function(apiParams, req, res, next) {

  const defaultCrs = "EPSG:4326"
  let datasetSettings = apiParams.config.datasets[apiParams.dataset]
  let crs = getParam(apiParams.svg_proj, datasetSettings.recommendedProjections[0], defaultCrs)
  crs = apiParams.config.crss_aliases[crs] || crs
  if (!(crs in epsg)){
    res.status(404).send(`Sorry, the requested projection "${crs}" is not known to Thenmap.`)
    return
  }

  let urlParams = {
    geo_props: apiParams.svg_props,
    language: apiParams.language,
    geo_crs: crs,
    //variant: //FIXME
  }

  callHome(req, "v2", apiParams.dataset, "geo", apiParams.date, urlParams, (err, body) => {
    if (err){
      console.log("Error", err)
    }
    // Calculate bounding bbox
    let extents = datasetSettings.bbox

    let bottomLeft = convert(extents.slice(0, 2), "EPSG:4326", crs)
    let [farLeft, farBottom] = bottomLeft
    let topRight = convert(extents.slice(2, 4), "EPSG:4326", crs)
    let [farRight, farTop] = topRight
    if (Math.sign(extents[1]) !== Math.sign(extents[3])){
      // If we span the equator, min/max coords could be well outside the bbox
      farLeft = convert([extents[0], 0], "EPSG:4326", crs)[0]
      farRight = convert([extents[2], 0], "EPSG:4326", crs)[0]
    }
    if (Math.sign(extents[0]) !== Math.sign(extents[2])){
      farBottom = convert([0, extents[1]], "EPSG:4326", crs)[1]
      farTop = convert([0, extents[3]], "EPSG:4326", crs)[1]
    }

    let width = getParam(apiParams.svg_width, 600)
    let height = getParam(apiParams.svg_height, 600)
    /* Round up dimension for a better chance of caching */
    width = Math.ceil(width / 20.0) * 20
    height = Math.ceil(height / 20.0) * 20

    let props  = urlParams.geo_props.split("|")

    // Produce SVG
    let svgStrings = []
    let converter = geojson2svg({
      output: 'svg',
      //mapExtent: ["left", "bottom", "right", "top"].reduce((o,x,i)=>(o[x]=extents[i], o), {})
      mapExtent: {
        "left": Math.min(bottomLeft[0], farLeft),
        "bottom": Math.min(bottomLeft[1], farBottom),
        "right": Math.max(topRight[0], farRight),
        "top": Math.max(topRight[1], farTop),
      },
      viewportSize: {
        width: width,
        height: height
      },
      //fitTo: "width",
      attributes: props.map(x=>"properties."+x),
      callback: arr=>{
        // Prepend each property with "thenmap:"
        svgStrings = props.reduce((a, x)=>a.map(y=>((x==="id" || !x)?y:y.replace(x, "thenmap:"+x))), arr)
      },
    })
    let geodata = JSON.parse(body)
    converter.convert(geodata)
    svgStrings = svgStrings.map(x=>x.replace(/(\d\.\d{4})\d+/g, "$1")) //truncate coords
    res.setHeader('Content-Type', 'image/svg+xml');
    return res.end(`<svg \
xmlns="http://www.w3.org/2000/svg" \
xmlns:thenmap="http://www.thenmap.net/2017/data" \
class="thenmap" \
preserveAspectRatio="xMidYMin meet" \
width="${width}" \
height="${height}" \
style="width: ${width}px; height: ${height}px; fill: gainsboro;" \
viewBox="0 0 ${width} ${height}" \
xmlns:xlink="http://www.w3.org/1999/xlink" \
version="1.1">\
<defs><pattern height="2" width="2" id="diagonalHatch" patternTransform="rotate(45 2 2)" patternUnits="userSpaceOnUse"><path d="M -1,2 l 6,0" stroke="gainsboro" stroke-width="2"></path></pattern><style>.limit{fill:url('#diagonalHatch')}</style></defs>
${svgStrings.join("\n")}
</svg>`)
  })  //callHome.geo
}
module.exports = svg
