const csv = require("fast-csv")
const path = require("path")
const {getParam} = require('../utils')

function translate(prop, fromDate, toDate, languages){
  if (!prop){
    return null
  }
  let translation = ""
  if (!Array.isArray(prop)){
    // if this is a value object, just return the first available translation
    let t = null
    languages.some(l => {
      // go through languages until we found a translation
      let found = false
      if (l in prop){
        t = prop[l]
        found = true
      } else if ("value" in prop && typeof prop.value === 'string'){
        t = prop.value
        found = true
      } else if ("value" in prop && l in prop.value){
        t = prop.value[l]
        found = true
      }
      return (found)
    })
    return ([t])
  } else {
    // if this is an array, filter out valid dates, and translate each
    translation = []
    prop.forEach(p => {
      let from_ = p["from"].toString() || "0"
      let to = p["to"].toString() || "9"
      if (!(from_ > toDate) && !(to < fromDate)) {
        let t = translate(p, null, null, languages)[0]
        translation.push(t)
      }
    })
    return translation
  }
  return []
}


let data = {}
data.get = function(apiParams, req, res, next) {

  let defaultLanguage = apiParams.config.datasets[apiParams.dataset].defaultLanguage
  let language = getParam(apiParams.language, defaultLanguage)
  let fallbackLanguage = apiParams.config.languageFallbacks[language]
  let date = apiParams.date
  let variants = apiParams.variant

  var dataPath = path.join(__dirname, "..", "..", "testdata", apiParams.dataset) + path.sep
  var dataCsv = dataPath + "1.csv"
  /* Get translated properties (synchronous) */
  var i18nJSON = require(dataPath + "i18n.json")

  let output = []

  csv.fromPath(dataCsv, {headers: true})
  .on('data', data => {
    // Apply filters
    // Filter by date
    if (data["edate"] < apiParams.fromDate || data["sdate"] >= apiParams.toDate) {
      return
    }
    // Filter by variant, if the dataset has them
    if ( ("version" in data) && ("versionset" in data) && data["versionset"] ){
      if ( data["versionset"] in variants ){
        if ( variants[data["versionset"]] !== data["version"] ){
          return
        }
      } else if (data["version"] > 1) {
          //Default: Only include 0 and 1
          return
      }
    }

    //Add entity
    let item = {}
    //const defaultProps = ["id"]
    //let data_props = [...new Set([...apiParams.data_props,...defaultProps])];
    let props = i18nJSON[data["id"]] || {}

    // If using a wildcard date; only include props that belong to this data-id,
    // not everything connected to the wikidata id
    let fromDate = apiParams.fromDate
    let toDate = apiParams.toDate
    if (fromDate === "0"){
      fromDate = data["sdate"]
    }
    if (toDate === "99"){
      toDate = data["edate"]
      if (toDate === "9"){
        toDate = "99"
      }
    }
    apiParams.data_props.forEach(requestedProp => {
      if ((requestedProp in data) || (requestedProp in props)){
        item[requestedProp] = data[requestedProp]
        if (requestedProp in props){
          let t = translate(props[requestedProp],
                            fromDate, toDate,
                            [language, fallbackLanguage, defaultLanguage])
          if (apiParams.fromDate === apiParams.toDate){
            if (t.length === 1) {
              // Don't use array for single items
              item[requestedProp] = t.pop()
            } else {
              item[requestedProp] = t
            }
            //item[requestedProp] = t.join(",")
          } else {
            item[requestedProp] = t
          }
        }
      }
    })
    output.push(item)
  })
  .on("end", function (){
    if (apiParams.data_format==="csv"){
      csv.writeToString(output,
        {headers: true}, (err, csvData) => {
          if (err){
            res.status(501).send('Failed converting data to csv: ' + err)
            return
          }
          res.writeHead(200, {
            'Content-Length': Buffer.byteLength(csvData),
            'Content-Type': "text/csv",
            'Content-Disposition': `filename=${apiParams.dataset}_${date.substring(0, 10) || "all"}_${language}.csv`,
          })
          res.write(csvData)
          return res.end()
        }
      )
    } else {
      return res.json(output)
    }
  })
  .on("error", function (err){
    res.status(501).send('Failed loading data: ' + err)
    return
  })

}

module.exports = data
