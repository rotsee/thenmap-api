var path = require("path")
var csv = require("fast-csv")

let info = {}
info.get = function(apiParams, req, res, next) {

	var output = apiParams.config.datasets[apiParams.dataset]
  delete(output.parameters) // fix me: Move to using ONLY parameters

  var dataPath = path.join(__dirname, "..", "..", "testdata", apiParams.dataset) + path.sep
  var dataCsv = dataPath + "1.csv"
  var i18nCsv = dataPath + "i18n.json"
  var i18nJSON = require(i18nCsv)

  /* Add i18n props */
  var i18nProps = new Set()
  for (var key in i18nJSON) {
    for (var propName of Object.keys(i18nJSON[key])){
      i18nProps.add(propName)
    }
  }
  /* Add static props */
  csv.fromPath(dataCsv, {headers: true})
   .on('data', function (data){
     for (var propName of Object.keys(data)){
       i18nProps.add(propName)
     }
   })
   .on('end', function(){
     output["data_props"] = Array.from(i18nProps)
     return res.json(output)
   })

}

module.exports = info
