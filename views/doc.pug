extends layout
block vars
   - var title = "Thenmap API v2"

block content
  header
    .back #[a(href="//www.thenmap.net") &larr; Home] | #[a(href="//api.thenmap.net/doc/v1") Version 1]
    h1 #{title}
  p.lead  This documentation refers to version 2 of the Thenmap API. See #[a(href="http://jplusplus.org/en/blog/version-two-of-the-thenmap-api/") this blog post] for a list of things that have changed since #[a(href="//api.thenmap.net/doc/v1") version 1].

  p General syntax:
  pre api-version/dataset/modules/date?language=XX
  
  p Each module has a set of specific url parameters, aside from the global #[a(href="#languages") #[code language]] parameter, #[a(href="#modules") see below].

  p Here is an example, returning geodata for Swedish municipalities as of 1982:
  pre <a href="/v2/se-7/geo/1982">v2/se-7/geo/1982</a>

  h2#version API version
  pre <b>api-version</b>/dataset/modules/date

  p Available versions are:
  
  ol
    li #[code v1], see the #[a(href="//api.thenmap.net/doc/v1") v1 documentation]
    li #[code v2], documented on this page

  h2#datasets Datasets
  pre version/<b>dataset</b>/modules/date

  p We name our datasets like this: <code>area-level</code>. For the area, we use <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2">ISO 3166</a> codes where available, and for level, we use the ten levels of administrative borders from <a href="http://wiki.openstreetmap.org/wiki/Template:Admin_level_10">Open Streetmap</a>. In other words <code>yd-4</code> would be the governorates of <a href="https://en.wikipedia.org/wiki/South_Yemen">South Yemen</a>. Not that we have them (yet), but <a href="mailto:stockholm@jplusplus.org">let us know</a> if you need them!

  each props, dataset in pageData.datasets
    h3 #{dataset}
    p #{props.description}
    p Languages: 
      each lang in pageData.datasets[dataset].languages
        <a href="http://www.loc.gov/standards/iso639-2/php/langcodes_name.php?iso_639_1=#{lang}"><code>#{lang}</code></a>.
      span The default language is <code>#{pageData.datasets[dataset].defaultLanguage}</code>.
    p Recommended projections:
      each proj in pageData.datasets[dataset].recommendedProjections
        code #{proj}

  h2#modules Modules
  pre version/dataset/<b>modules</b>/date

  p Available modules are: !{pageData.modules.map(function(x){return "<a href='#"+x+"'><code>"+x+"</code></a>"}).join(" &middot; ")}.

  each props, module in pageData.modulesettings
    h3(id=module) #{module}
    p #{props.description}
    if 'parameters2' in props
      each methodparams, method in props.parameters2
        p Method: #[code #{method}]
        if Object.keys(props.parameters2[method]).length
          p Url parameters:
          dl
          each paramprops, param in props.parameters2[method]
            dt
              if paramprops.deprecated
                code.deprecated #{param}
              else
                code #{param}
            dd
              p #{paramprops.description}
              dl
                if 'default' in paramprops
                  dt Default:
                  dd
                    if paramprops.default == ""
                      i empty
                    code #{paramprops.default}
                if 'allowed' in paramprops
                  dt Allowed:
                  dd
                    code #{paramprops.allowed}

  h2#date Date
  pre version/dataset/modules/<b>date</b>

  p Can be on the form <code>YYYY-MM-DD</code>, <code>YYYY-MM</code>, or <code>YYYY</code>. The output will include only areas that existed at that very date. Dates in the database are local time and inclusive, meaning that #[code 1947-03-01] will include a nation that gained independence on April 1 1947, according to the local timezone. In reality these dates are often up for debate, and different definitions of independence will be a much bigger source of ambiguity than time zones.
  p If you need all available areas at once (e.g. for creating an #[a(href="http://old.thenmap.net/") animated map]), you can use the wildcard #[code *]. This is equivalent to leaving the date out in #[a(href="/doc/v1") version 1] of the API.
  p <code>2011-05</code> will be interpreted as <code>2001-05-01</code>, and <code>2011</code> will be interpreted as <code>2001-01-01</code>. Technically speaking all dates that the ECMAScript 2016 #[a(href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/parse#Examples") #[code Date()]] contructor can parse could be used (e.g. #[code May 1978] or #[code 2017-12-20T20:11]), but this is not guaranteed to work forever.

  h2#languages Languages
  p Some properties of the datasets, such as names of political entities, are automatically translated using <a href="https://www.wikidata.org">WikiData</a>. When a translation is missing, the following fallback rules are applied, before falling back to the default language of the dataset, and lastly, to English:
  each to, from in pageData.languageFallbacks
    ul
      li #{from} → #{to}
  p If nothing else is stated, these rules apply to all modules that use the <code>language</code> parameter.

  h2#variants Dataset variants
  p Datasets may contain different versions of borders. For instance, it might in some cases be useful to show Somaliland as a nation, given its <i>de facto</i> indepence, where as in other cases you might want to show its <i>de jure</i> status as part of Somalia.
  p Variants are not yet implemented in version 2 of the API. If you need them, please keep using #[a(href="/dok/v1") version 1] until they are in place.
  p Let us know if you have any specific need for versioning!

  h2#examples Examples

  mixin example(uri)
    code
      a(href=uri)= uri

  dl
    dt SVG image of the world 2015
    dd
      +example("/v2/world-2/svg/2015")

    dt All national borders as topojson, and their start and end date
    dd
      +example("/v2/world-2/geo/*/?data_props=sdate|edate&geo_type=topojson")

    dt All Swedish municipalities and their areas and names, in 1974, in Sami language
    dd
      +example("/v2/se-7/data/1974?data_props=area|name&language=se")

    dt Swedish municipalities in June 1978 as geojson, using the RT90 coordinate system
    dd
      +example("/v2/se-7/geo/1978-06-01?geo_crs=RT90")

    dt Metadata on the Swedish municipalities dataset
    dd
      +example("/v2/se-7/info/*")

    dt Borders of the world on May 4th, 1994, using a specific variant of a subset of the world-2 dataset
    dd
      +example("/v1/world-2/geo/1994-05-04?geo_variant=somalia=2")
