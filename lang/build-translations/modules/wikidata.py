# -*- coding: utf-8 -*-
"""Read various WikiData data"""

from wikitools.wiki import Wiki
from wikitools.api import APIRequest

PROLEPTIC_GREGORIAN = "http://www.wikidata.org/entity/Q1985727"
GREGORIAN = "https://www.wikidata.org/wiki/Q12138"
PROLEPTIC_JULIAN = "https://www.wikidata.org/wiki/Q1985786"
JULIAN = "https://www.wikidata.org/wiki/Q11184"
"""Calendar types"""


class Qid(object):
    """ Store a Wikidata id (eg "Q1234") """
    _id = ''

    def __init__(self, code):
        self.qid = code

    def __str__(self):
        return self.qid

    def __unicode__(self):
        return u"%s" % self.qid

    def numerical(self):
        """ Return numerical part of id """
        return int(self.qid[1:])

    @property
    def qid(self):
        """Return Wikidata ID as string"""
        return self._id

    @qid.setter
    def qid(self, code):
        """Update Wikidata ID"""
        if isinstance(code, str):
            if 'Q' == code[0]:
                self._id = code
            else:
                self._id = "Q" + code
        else:
            self._id = "Q" + str(int(round(float(code))))


class WikimediaCommons(object):
    """ Class to access the Wikimedia Commons API
    """

    api_url = "https://commons.wikimedia.org/w/api.php"
    chunk_size = 49  # Number of id's to query for in each request
    site = None

    def __init__(self, username, password):
        """Will login the user"""
        self.site = Wiki(self.api_url)
        self.site.login(username, password)

    def get_filepaths(self, file_names):
        results = {}
        file_names = list(set(file_names))  # pick only unique values
        file_names = [u"File:" + x for x in file_names if x not in ["", None]]
        params = {u'action': u"query",
                  u'prop': u"imageinfo",
                  u'iiprop': u"url",
                  u'format': u"json",
                  }
        chunks = [file_names[x:x + self.chunk_size]
                  for x in xrange(0, len(file_names), self.chunk_size)]
        for chunk in chunks:
            params[u'titles'] = '|'.join(chunk)
            request = APIRequest(self.site, params)
            result = request.query()
            for pageid, data in result["query"]["pages"].iteritems():
                name = data["title"]
                if "imageinfo" in data:
                    results[name] = data["imageinfo"][0]["url"]
        return results


class WikiData(object):
    """This class is a wrapper around some common WikiData tasks.
    """

    api_url = "https://www.wikidata.org/w/api.php"
    chunk_size = 49  # Number of id's to query for in each request
    site = None
    username = None
    password = None

    CLAIM_RANKS = {
        'preferred': 3,
        'normal': 2,
        'deprecated': 1
    }

    def __init__(self, username, password):
        """Will login the user"""
        self.username = username
        self.password = password
        self.site = Wiki(self.api_url)
        self.site.login(username, password)

    def _get_entities(self, ids, props, languages=None):
        """Wrapper around action=wbgetentities
        Returns a dict like {QID: {entity_obj}, ...}
        """
        results = {}
        ids = list(set(ids))  # pick only unique values
        ids = [x for x in ids if x not in ["Q-1", "Q0"]]  # remove invalid qids
        params = {'action': "wbgetentities",
                  'props': props,
                  'format': "json"}
        if languages is not None:
            params['languages'] = "|".join(languages)
        chunks = [ids[x:x + self.chunk_size]
                  for x in xrange(0, len(ids), self.chunk_size)]
        for chunk in chunks:
            params['ids'] = '|'.join(chunk),
            request = APIRequest(self.site, params)
            result = request.query()
            for qid, entity in result["entities"].iteritems():
                results[qid] = entity
        return results

    def _get_best_claims(self, claims):
        """Return the highest ranked claims
        """
        available_claims = set()
        # Find all rank values
        for claim in claims:
            rankval = self.CLAIM_RANKS[claim["rank"]]
            available_claims.add(rankval)
        max_rank = max(available_claims)
        # Filter out claims with highest rank
        best_claims = [c for c in claims
                       if self.CLAIM_RANKS[c["rank"]] == max_rank]
        return best_claims

    def get_translations(self, entities, languages):
        """Return the labels for each language"""
        output = {}
        result = self._get_entities(entities, "labels", languages)

        for (qid, entity) in result.iteritems():
            output[qid] = {}
            try:
                for (language_code, label) in entity["labels"].iteritems():
                    output[qid][language_code] = label["value"]
            except KeyError:
                print "no labels for %s" % qid

        return output

    def get_properties(self, entities, req_claims,
                       req_qualfiers=None, only_best_claims=False):
        """Return the values of certain claims for an entity
           that will be either a list of QID's, or a list of
           strings.
           Only claims with the highest rank will be returned,
           in other words: If there is at least one claim with
           the rank `preferred` (the highest rank), then only
           preferred claims will be returned.
           Only `snaktype == value` claims will be returned, ie
           there must be a known, non empty value.
           If there are multiple qualifyers of the type requested
           only the first will be returned
        """
        output = {}
        result = self._get_entities(entities, "claims")

        for (qid, entity) in result.iteritems():
            output[qid] = {}
            try:
                for (claim_id, claims) in entity[u"claims"].iteritems():
                    if claim_id in req_claims:
                        if only_best_claims:
                            claims = self._get_best_claims(claims)

                        # Include only datavalue and qualifiers if requested
                        cleaned_claims = []
                        for claim in claims:
                            try:
                                if claim[u"mainsnak"][u"snaktype"] == u"value":
                                    clean_claim = {
                                        'datavalue': claim[u"mainsnak"][u"datavalue"],
                                        'datatype': claim[u"mainsnak"][u"datatype"],
                                    }
                                    if (req_qualfiers is not None and
                                            u"qualifiers" in claim):
                                        qualifiers = {}
                                        for qualifier_id, qualifier_list in claim[u"qualifiers"].iteritems():
                                            if ((qualifier_id in req_qualfiers) and
                                                u"datavalue" in qualifier_list[0]):
                                                qualifiers[qualifier_id] = qualifier_list[0][u"datavalue"]
                                        if len(qualifiers):
                                            clean_claim["qualifiers"] = qualifiers
                                    cleaned_claims.append(clean_claim)
                                    output[qid][claim_id] = cleaned_claims
                            except KeyError:
                                print "odd claim for %s:" % qid
                                print claim
            except KeyError:
                print "missing claims for %s" % qid

        return output

    def _get_nice_value(self, datavalue):
        """ Return a human readable version of a datavalue,
            or a qid if the property time is a WikiData item.
        """
        type_ = datavalue[u"type"]

        # Case "wikibase-entityid"
        if type_ == "wikibase-entityid":
            return Qid(datavalue[u"value"][u"numeric-id"]).qid
        # Case "time"
        elif type_ == "time":
            time = datavalue[u"value"][u"time"]
            if datavalue[u"value"][u"calendarmodel"] == PROLEPTIC_GREGORIAN:
                from datetime import datetime
                # +1779-00-00T00:00:00Z dates are not valid here
                time = time.replace("-00-00T", "-01-01T")
                time = time.replace("-00T", "-01T")
                time_obj = datetime.strptime(time, "+%Y-%m-%dT%H:%M:%SZ")
                timestring = time_obj.isoformat()
                return timestring
            else:
                return time
        elif type_ == "string":
            return datavalue[u"value"]
        # Case "url"
        elif type_ == "url":
            return datavalue[u"value"]
        else:
            print("Unknown datatype: {}".format(type_))
        return datavalue[u"value"]

    def get_nice_values(self, entities,
                        req_claims, languages,
                        req_qualfiers=None):
        """ Like get_properties, but will attempt to return human readable
        representations (kind of) of each property and qualifier.
        The returned format depends on the data type of the value.
          * Commons media: media url (string)
          * Monolingual text: text (string)
          * String: text (string)
          * Item: dictionary of labels in requested languages
          * Time: ISO 8601, Gregorian (string)
        When needed the Wikimedia Commons API will be called to request URL's
        """
        properties = self.get_properties(entities, req_claims, req_qualfiers)
        output = {}
        qids_to_translate = []  # WD items need to be looked up separately
        files_to_lookup = []  # File paths need to be looked up separately

        # Loop through each prop of proplist of each entity
        for qid, all_props in properties.iteritems():
            output[qid] = {}
            for prop_id, prop_list in all_props.iteritems():
                output[qid][prop_id] = []
                for prop in prop_list:
                    type_ = prop[u"datavalue"][u"type"]

                    # Get qualifiers
                    qualifiers = {}
                    if "qualifiers" in prop:
                        for qualifier_id, qualifier_val in prop["qualifiers"]\
                                                           .iteritems():
                            qualifiers[qualifier_id] = self._get_nice_value(
                                                                qualifier_val)

                    # Get actual value
                    value = self._get_nice_value(prop[u"datavalue"])
                    # Save WD entities for later lookup
                    if type_ == "wikibase-entityid":
                        qids_to_translate.append(value)
                    # Save file names for later lookup
                    if prop[u"datatype"] == "commonsMedia":
                        files_to_lookup.append(value)

                    output[qid][prop_id].append({
                        "qualifiers": qualifiers,
                        "value": value
                    })

        # Get translated names of wikibase-entityid type entities
        translated_qids = self.get_translations(qids_to_translate, languages)

        # Get urls for files on commons
        wikimediaCommons = WikimediaCommons(self.username, self.password)
        file_paths = wikimediaCommons.get_filepaths(files_to_lookup)

        # Loop through each prop of proplist of each entity again
        # to insert translated values, and file paths
        for qid, all_props in output.iteritems():
            for prop_id, prop_list in all_props.iteritems():
                for prop in prop_list:
                    if prop["value"] in translated_qids:
                        prop["value"] = translated_qids[prop["value"]]
                    elif "File:" + prop["value"].replace('_', ' ') in file_paths:
                        # sometimes WD returns underscore in values. Commons don't
                        prop["value"] = file_paths["File:" + prop["value"].replace('_', ' ')]

        return output

if __name__ == "__main__":
    print "This module is only intended to be called from other scripts."
    import sys
    sys.exit()
