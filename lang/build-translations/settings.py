"""Settings for the build-translations scripts. This should
   be integrated with the rest of the Thenmap data ecosystem.
"""

import login
from os import path as os_path
from os import sep as os_sep

CURRDIR = os_path.dirname(os_path.realpath(__file__))
DATADIR = os_sep.join([CURRDIR, "..", "..", "testdata"])
OVERRIDESDIR = os_sep.join([CURRDIR, "..", "local-overrides"])
"""Where csv files for tabular data is stored"""

DATASETS = {
    'ch-8': {
        'version': 1,
        'languages': ["als", "frp", "lmo", "de", "fr", "it", "rm", "en", "sq", "es", "tr", "pt"],
        'entityProperties': {'is_in': "P131",
                             'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'languages': "P37",
                             'borders': "P47",
                             },
    },
    'fi-8': {
        'version': 1,
        'languages': ["sv", "fi", "en", "ru", "se", "et", "no"],
        'entityProperties': {'is_in': "P131",
                             'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'borders': "P47",
                             },
    },
    'no-7': {
        'version': 1,
        'languages': ["nb", "nn", "en", "se", "fi", "da"],
        'entityProperties': {'is_in': "P131",
                             'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'municipality_number': "P1168",
                             'borders': "P47",
                             },
    },
    'no-4': {
        'version': 1,
        'languages': ["nb", "nn", "en", "se", "fi", "da"],
        'entityProperties': {'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'flag': "P41",
                             'NUTS': "P605",
                             'ISO': "P300",
                             },
    },
    'dk-7': {
        'version': 1,
        'languages': ["da", "de", "en", "sv", "ffr", "nds", "fo"],
        'entityProperties': {'is_in': "P131",
                             'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'flag': "P41",
                             'municipality_number': "P2504",
                             'borders': "P47",
                             },
    },
    'se-7': {
        'version': 1,
        'languages': ["sv", "en", "fi", "se"],
        'entityProperties': {'is_in': "P131",
                             'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'borders': "P47",
                             },
    },
    'se-4': {
        'version': 1,
        'languages': ["sv", "en", "fi", "se"],
        'entityProperties': {'has_capital': "P36",
                             'coat_of_arms': "P94",
                             'borders': "P47",
                             },
    },
    'us-4': {
        'version': 0,
        'languages': ["sv", "en", "es", "zh", "fr", "de", "it", "nl", "tl", "vi", "ik", "ko", "ru", "fa", "nv", "th", "chr", "ar"],
        'entityProperties': {'has_capital': "P36",
                             'flag': "P41",
                             'coat_of_arms': "P94",
                             'borders': "P47",
                             },
    },
    'gl-7': {
        'version': 1,
        'languages': ["kl", "da", "sv", "no", "en"],
        'entityProperties': {'has_capital': "P36",
                             'coat_of_arms': "P94",
                             },
    },
    "world-2": {
        'version': 1,
        'languages': ["sv", "en", "fi", "fr", "de", "es", "ru", "it", "nl", "pl", "zh", "pt", "ar", "ja", "fa", "no", "he", "tr", "da", "uk", "ca", "id", "hu", "vi", "ko", "et", "cs", "hi", "sr", "bg", "nn"],
        'entityProperties': {'capital': "P36",
                             'currency': "P38",
                             'government': "P122",
                             'flag': "P41",
                             'borders': "P47",
                             },
    },
}
"""What entities should we get from Wikidata?"""
