#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Will create a json file with localized name of properties for
 each political entity in a dataset, fetching data from WikiData,
 and local overrides.

 This program will:
 * Loop through all shape tables for WikiData id:s
 * Fetch data from WikiData
 * Ferch local override data
 * Create json objects like this:
    # id: {name: [{name,from,to,...},...], capital: [...]}, ...
"""
import settings

from modules.interface import Interface
from modules.wikidata import WikiData
from modules.wikidata import Qid
from os import sep as os_sep
from json import dump as json_dump
from csv import DictReader
from copy import deepcopy


def main():
    """ Entry point when run from command line
    """
    # pylint: disable-msg=C0103

    cmd_args = [
        {
            'short': "-d", "long": "--dataset",
            'dest': "dataset",
            'type': str,
            'help': "Dataset, eg `se-7` or `world-2`",
            'required': True
        },
        {
            'short': "-t", 'long': "-test",
            'dest': "dryrun",
            'type': bool,
            'choices': (0, 1),
            'default': 0,
            'help': "Do a dryrun, only printing to the screen?"
        }
    ]
    ui = Interface("Build Translations",
                   "Fetch property names from WikiData",
                   commandline_args=cmd_args)

    try:
        data_settings = settings.DATASETS[ui.args.dataset]
    except KeyError:
        ui.error("No such dataset: " + ui.args.dataset)
        ui.exit()

    # Fetch wikidata id's for all political entities
    qid_dict = {}  # {QID: [ids]}
    id_dict = {}  # {id: [qids]}
    output_dict = {}  # {id: {data}}

    dataset_path = os_sep.join([settings.DATADIR, ui.args.dataset]) + os_sep
    dataset_name = dataset_path + str(data_settings["version"]) + ".csv"
    ui.debug("Using dataset file %s" % dataset_name)
    overrides_name = settings.OVERRIDESDIR + os_sep + ui.args.dataset + ".csv"
    ui.debug("Using local overrides from %s" % overrides_name)
    try:
        with open(dataset_name) as csv_file:
            csv_reader = DictReader(csv_file)
            for row in csv_reader:
                # Add id to this qid
                # print row
                qid = Qid(row["wikidata"]).qid
                qid_dict.setdefault(qid, []).append(row["id"])
                id_dict.setdefault(row["id"], []).append(qid)
    except IOError:
        ui.error("Could not open " + dataset_name)

    wd = WikiData(settings.login.user, settings.login.password)

    # Query Wikidata for labels (translations)
    translations = wd.get_translations(qid_dict.keys(),
                                       data_settings["languages"])
    # Put WikiData data in output dictionary
    for qid, translation in translations.iteritems():
        ids = qid_dict[qid]
        for id_ in ids:
            output_dict[id_] = {
                # We need to deepcopy here, or we'll get trouble when the same
                # WD item is shared among multiple thenmap items, but one of
                # the thenmap items has a local override,
                # e.g. se-4:Dalarna county and se-4:Kopparberg county
                "name": deepcopy(translation),
            }

    # Query Wikidata for additional properties
    properties = wd.get_nice_values(qid_dict.keys(),
                                    data_settings["entityProperties"].values(),
                                    data_settings["languages"],
                                    req_qualfiers=["P580", "P582"])  # from/to

    # Insert additional properties
    for id_, values in output_dict.iteritems():
        qids = id_dict[id_]
        for qid in qids:
            if qid in properties:
                for prop_name, prop_qid in data_settings["entityProperties"].iteritems():
                    if prop_qid in properties[qid]:
                        list_ = []
                        for val in properties[qid][prop_qid]:
                            obj_ = {"value": val["value"]}
                            if "P580" in val["qualifiers"]:
                                obj_["from"] = val["qualifiers"]["P580"]
                            else:
                                obj_["from"] = 0
                            if "P582" in val["qualifiers"]:
                                obj_["to"] = val["qualifiers"]["P582"]
                            else:
                                obj_["to"] = 9
                            list_.append(obj_)
                        output_dict[id_][prop_name] = list_

    # Get local overrides for name property
    try:
        with open(overrides_name) as csv_file:
            csv_reader = DictReader(csv_file)
            for row in csv_reader:
                id_ = row.pop("id", None)
                ui.debug("Found overrides for %s" % id_)
                if id_ not in output_dict:
                    output_dict[id_] = {"name": {}}
                for lang, string in row.iteritems():
                    if string:
                        # ui.debug("Local override for %s: %s" % (lang, string))
                        output_dict[id_]["name"][lang] = string
    except IOError:
        ui.error("Could not open " + overrides_name)

    # save json
    if ui.args.dryrun:
        ui.info("Running in normal mode would have written the following to %s:" % (dataset_path + "i18n.json"))
        print output_dict
    else:
        with open(dataset_path + "i18n.json", 'w') as output_file:
            ui.debug("Writing json to %s" % (dataset_path + "i18n.json"))
            json_dump(output_dict, output_file)

if __name__ == '__main__':
    main()
